﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace bivaa_server.Controllers
{
    public class StockItemController : ApiController
    {
        // GET: api/StockItem
        public HttpResponseMessage Get()
        {
            List<stock_item> result;
            using (var db = new AppDbModel())
            {
                var stockItems = from s in db.stock_item select s;
                result = stockItems.OrderByDescending(o => o.name).ToList();
                var jsonString = JsonConvert.SerializeObject(result);

                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(jsonString)
                };
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                return resp;
            }
        }

        // GET: api/StockItem/5
        public HttpResponseMessage Get(int id)
        {
            stock_item stockItem;
            using (var db = new AppDbModel())
            {
                stockItem = (from s in db.stock_item where s.id == id select s).SingleOrDefault();
                var jsonString = JsonConvert.SerializeObject(stockItem);

                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(jsonString)
                };
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                return resp;
            }
        }

        // POST: api/StockItem
        public void Post(HttpRequestMessage value)
        {
            var stockItem = JsonConvert.DeserializeObject<stock_item>(value.Content.ReadAsStringAsync().Result);
            using (var db = new AppDbModel())
            {
                db.stock_item.Add(stockItem);
                db.SaveChanges();
            }
        }

        // PUT: api/StockItem/5
        public void Put(int id, HttpRequestMessage value)
        {
            var requestStockItem = JsonConvert.DeserializeObject<stock_item>(value.Content.ReadAsStringAsync().Result);
            using (var db = new AppDbModel())
            {
                var stockItem = (from s in db.stock_item where s.id == id select s).SingleOrDefault();
                stockItem.code = requestStockItem.code;
                stockItem.name = requestStockItem.name;
                stockItem.sell_price = requestStockItem.sell_price;
                db.SaveChanges();
            }
        }

        // DELETE: api/StockItem/5
        public void Delete(int id)
        {
            using (var db = new AppDbModel())
            {
                var stockItem = (from s in db.stock_item where s.id == id select s).SingleOrDefault();
                db.stock_item.Remove(stockItem);
                db.SaveChanges();
            }
        }
    }
}
