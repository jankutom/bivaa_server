﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace bivaa_server.Controllers
{
    public class SalesOrderController : ApiController
    {
        // GET: api/SalesOrder
        public HttpResponseMessage Get()
        {
            List<sales_order> result;
            using (var db = new AppDbModel())
            {
                var salesOrders = from s in db.sales_order select s;
                result = salesOrders.OrderByDescending(o => o.id).ToList();
                var jsonString = JsonConvert.SerializeObject(result);

                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(jsonString)
                };
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                return resp;
            }
        }

        // GET: api/SalesOrder/5
        public HttpResponseMessage Get(int id)
        {
            sales_order salesOrder;
            using (var db = new AppDbModel())
            {
                salesOrder = (from s in db.sales_order where s.id == id select s).SingleOrDefault();
                var jsonString = JsonConvert.SerializeObject(salesOrder);

                var resp = new HttpResponseMessage()
                {
                    Content = new StringContent(jsonString)
                };
                resp.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
                return resp;
            }
        }

        // POST: api/SalesOrder
        public void Post(HttpRequestMessage value)
        {
            var salesOrder = JsonConvert.DeserializeObject<sales_order>(value.Content.ReadAsStringAsync().Result);
            using (var db = new AppDbModel())
            {
                db.sales_order.Add(salesOrder);
                db.SaveChanges();
            }
        }

        // PUT: api/SalesOrder/5
        public void Put(int id, HttpRequestMessage value)
        {
            var requestSalesOrder = JsonConvert.DeserializeObject<sales_order>(value.Content.ReadAsStringAsync().Result);
            using (var db = new AppDbModel())
            {
                var salesOrder = (from s in db.sales_order where s.id == id select s).SingleOrDefault();

                // todo calculate price
                // todo proper sales order item saving
                salesOrder.sales_order_item = requestSalesOrder.sales_order_item;
                salesOrder.brand = requestSalesOrder.brand;
                salesOrder.customer = requestSalesOrder.customer;

                db.SaveChanges();
            }
        }

        // DELETE: api/SalesOrder/5
        public void Delete(int id)
        {
            using (var db = new AppDbModel())
            {
                var salesOrder = (from s in db.sales_order where s.id == id select s).SingleOrDefault();
                db.sales_order.Remove(salesOrder);
                db.SaveChanges();
            }
        }
    }
}
